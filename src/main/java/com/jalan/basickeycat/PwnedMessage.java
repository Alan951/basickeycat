package com.jalan.basickeycat;

import java.io.Serializable;

public class PwnedMessage implements Serializable{

	private String action;
	private String result;
	private Object data;
	private C2Message c2Message;
	
	public PwnedMessage(String action, String result, Object data, C2Message c2Message) {
		super();
		this.action = action;
		this.result = result;
		this.data = data;
		this.c2Message = c2Message;
	}
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public C2Message getC2Message() {
		return c2Message;
	}
	public void setC2Message(C2Message c2Message) {
		this.c2Message = c2Message;
	}

	@Override
	public String toString() {
		return "PwnedMessage [action=" + action + ", result=" + result + ", data=" + data + ", c2Message=" + c2Message
				+ "]";
	}	
}
