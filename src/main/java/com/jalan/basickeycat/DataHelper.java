package com.jalan.basickeycat;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class DataHelper {
	
	public static OutputStream os;
	public static ByteArrayOutputStream baos;

	public static synchronized void write(byte[] data, OutputStream fos) throws IOException {
		fos.write(data);
	}
	
}
