package com.jalan.basickeycat;

import java.io.Serializable;

public class C2Message implements Serializable{

	public String action;
	public String cmd;
	public String data;
	
	public C2Message(String action, String cmd, String data)  {
		super();
		this.action = action;
		this.cmd = cmd;
		this.data = data;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		return "C2Message [action=" + action + ", cmd=" + cmd + ", data=" + data + "]";
	}
}
